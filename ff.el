;;; ff.el --- Interact with Firefox -*- lexical-binding: t -*-

;; Copyright 2018, Steckerhalter

;; Author: steckerhalter
;; Package-Requires: ((esqlite "0") (helm "0"))
;; Keywords: firefox interact url tabs
;; URL: https://github.com/steckerhalter/ff.el

;; This file is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;; <http://www.gnu.org/licenses/>.

;;; Code:

(require 'json)
(require 'esqlite)
(require 'helm)

(defgroup ff.el nil
  "Interact with Firefox."
  :group 'external)

(defcustom ff-lz4-executable "mozlz4a-decompress"
  "Executable to decompress jsonlz4 file."
  :type 'string)

(defcustom ff-profiles-path "~/.mozilla/firefox"
  "Path to where profiles.ini is located."
  :type 'string)

(defcustom ff-profile "Profile0"
  "Firefox profile to use."
  :type 'string)

(defcustom ff-session-store "sessionstore-backups/recovery.jsonlz4"
  "Relative path the session store backup."
  :type 'string)

;; from https://github.com/daniel-ness/ini.el
(defun ff-ini-decode (ini_text)
  "Decode .ini to an alist."
  (interactive)
  (if (not (stringp ini_text))
      (error "Must be a string"))
  (let ((lines (split-string ini_text "\n"))
        (section)
        (section-list)
        (alist))
    (dolist (l lines)
      ;; skip comments
      (unless (or (string-match "^;" l)
                  (string-match "^[ \t]$" l))
        ;; catch sections
        (if (string-match "^\\[\\(.*\\)\\]$" l)
            (progn
              (if section
                  ;; add as sub-list
                  (setq alist (cons `(,section . ,section-list) alist))
                (setq alist section-list))
              (setq section (match-string 1 l))
              (setq section-list nil)))
        ;; catch properties
        (if (string-match "^\\([^\s\t]+\\)[\s\t]*=[\s\t]*\\(.+\\)$" l)
            (let ((property (match-string 1 l))
                  (value (match-string 2 l)))
              (progn
                (setq section-list (cons `(,property . ,value) section-list)))))))
    (if section
        ;; add as sub-list
        (setq alist (cons `(,section . ,section-list) alist))
      (setq alist section-list))
    alist))

(defun ff-get-profile-path ()
  "Get the path where the Firefox profiles are located."
  (let ((data (ff-ini-decode (with-temp-buffer
                               (insert-file-contents
                                (expand-file-name "profiles.ini" ff-profiles-path))
                               (buffer-string)))))
    (expand-file-name
     (cdr (assoc-string "Path" (assoc-string ff-profile data)))
     ff-profiles-path)))

(defun ff-get-session-data ()
  "Get info about the Firefox session from recovery file."
  (let ((json-object-type 'plist)
        (json-array-type 'list))
    (json-read-from-string
     (shell-command-to-string
      (concat ff-lz4-executable " " (expand-file-name ff-session-store (ff-get-profile-path)))))))

(defun ff-get-current-url ()
  "Retrieve the current url from the session data."
  (let* ((data (car (plist-get (ff-get-session-data) :windows)))
         (selected (plist-get data :selected))
         (tabs (plist-get data :tabs)))
    (nth (- selected 1) (mapcar (lambda (tab)
                                  (plist-get (nth (- (plist-get tab :index) 1)
                                                  (plist-get tab :entries)) :url)) tabs))))
;;;###autoload
(defun ff-paste-current-url ()
  "Paste the current Firefox url."
  (interactive)
  (insert (ff-get-current-url)))

(defvar ff-places nil "Variable to store the places.")

(defun ff-helm-build-source (desc db sql)
  "Build Helm source with desciption DESC, database DB and query SQL."
  (eval
   `(helm-build-sync-source ,(format "ff.el - %s" desc)
      :init (lambda ()
              (setq ff-places
                    (esqlite-read
                     (expand-file-name ,db (ff-get-profile-path))
                     ,sql)))
      :candidates (lambda ()
                    (mapcar (lambda (place)
                              (let ((url (car place))
                                    (title (unless (eq (cadr place) :null)
                                             (cadr place))))
                                (cons
                                 (concat
                                  (propertize url 'face font-lock-builtin-face)
                                  (when title (concat " " title)))
                                 (list url title))))
                            ff-places))
      :action (helm-make-actions
               "Browse Url"
               (lambda (candidate)
                 (helm-browse-url (car candidate)))
               "Copy Url"
               (lambda (candidate)
                 (kill-new (car candidate))
                 (message "`%s' copied to kill-ring" (car candidate)))))))

(defcustom ff-helm-places-query
  "select url, title from moz_places where last_visit_date is not null order by last_visit_date desc"
  "sql query to get places from Firefox db."
  :type 'string)

;;;###autoload
(defun ff-helm-places ()
  "Filter Firefox history and bookmarks with Helm."
  (interactive)
  (helm :sources (ff-helm-build-source
                  "Firefox bookmarks and history"
                  "places.sqlite"
                  ff-helm-places-query)
        :candidate-number-limit 9999
        :full-frame t
        :buffer "*Helm FF*"))

(defcustom ff-helm-bookmarks-query
  "select distinct p.url, p.title from moz_bookmarks b, moz_places p where b.fk = p.id and fk is not null and hidden != 1 order by position asc"
  "sql query to get bookmarks from Firefox db."
  :type 'string)

;;;###autoload
(defun ff-helm-bookmarks ()
  "Filter Firefox bookmarks with Helm."
  (interactive)
  (helm :sources (ff-helm-build-source
                  "Firefox bookmarks"
                  "places.sqlite"
                  ff-helm-bookmarks-query)
        :candidate-number-limit 9999
        :full-frame t
        :buffer "*Helm FF*"))

(provide 'ff)

;;; ff.el ends here
